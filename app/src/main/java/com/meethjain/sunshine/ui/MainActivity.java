package com.meethjain.sunshine.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.meethjain.sunshine.R;
import com.meethjain.sunshine.ui.fragments.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, MainFragment.newInstance(null), MainFragment.TAG)
                    .commit();
        }
    }
}

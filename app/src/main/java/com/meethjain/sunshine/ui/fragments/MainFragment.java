package com.meethjain.sunshine.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.meethjain.sunshine.R;
import com.meethjain.sunshine.common.ui.BaseFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created on 15/01/16.
 *
 * @author Meeth D Jain
 */
public class MainFragment extends BaseFragment {

    public static final String TAG = MainFragment.class.getSimpleName();

    @Bind(R.id.list_view_main_forecast)
    ListView mForecastListView;

    public static MainFragment newInstance(Bundle bundle) {
        MainFragment fragment = new MainFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }
}

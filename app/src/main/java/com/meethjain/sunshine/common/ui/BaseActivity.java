package com.meethjain.sunshine.common.ui;

import android.support.v7.app.AppCompatActivity;

/**
 * Created on 02/01/17.
 *
 * @author Meeth D Jain
 */
public abstract class BaseActivity extends AppCompatActivity {
}
